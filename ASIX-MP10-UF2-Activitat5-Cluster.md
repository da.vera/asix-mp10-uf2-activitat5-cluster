# ASIX-MP10-UF2-Activitat5-Cluster

## Clustering  MySQL

### Matèixa configuració tant pel management node com pels data nodes
Descarreguem MySQL amb wget i a continuació, extraiem el fitxer tar.

![Foto](/imgmysql/image001.png)

![Foto](/imgmysql/image003.png)

Abans d’instal·lar el paquet rpm a MySQL Cluster, hem d’instal·lar perl-Data-Dumper i eliminar mariadb-libs abans d’instal·lar MySQL Cluster.

![Foto](/imgmysql/image006.png)

Instal·lem el paquet MySQL Cluster amb el paquet rpm.

![Foto](/imgmysql/image010.png)

![Foto](/imgmysql/image012.png)

Afegim una contrasenya a l'usuari de la base de dades.

![Foto](/imgmysql/image014.png)

Creem un nou directori pels fitxers de configuració.

![Foto](/imgmysql/image017.png)

A continuació, creem un nou fitxer de configuració per a la gestió del clúster anomenada "config.ini" al directori del clúster mysql.

<code>cd /var/lib/mysql-cluster</code>
<code>vi config.ini</code>

Afegim els data nodes dins de la configuració del management node.

[ndb_mgmd default]<br>
Directory for MGM node log files<br>
DataDir=/var/lib/mysql-cluster<br>
 
[ndb_mgmd]<br>
Management Node db1<br>
HostName=10.92.254.121<br>
 
[ndbd default]<br>
NoOfReplicas=2      # Number of replicas<br>
DataMemory=256M     # Memory allocate for data storage<br>
IndexMemory=128M    # Memory allocate for index storage<br>
Directory for Data Node<br>
DataDir=/var/lib/mysql-cluster<br>
 
[ndbd]<br>
Data Node db2<br>
HostName=10.92.255.255<br>
 
[ndbd]<br>
Data Node db3<br>
HostName=10.92.255.166<br>
 
[mysqld]<br>
SQL Node db4<br>
HostName=10.92.255.151<br>
 
[mysqld]<br>
SQL Node db5<br>
HostName=10.92.255.161<br>

Hem de crear un fitxer de configuració per afegir els 2 data nodes:

![Foto](/imgmysql/image016.png)

![Foto](/imgmysql/image018.png)

Amb la següent comanda iniciem el node de dades:

![Foto](/imgmysql/image020.png)

Finalment mostrem que els dos data nodes están connectats al nostre management node.

<code>ndb_mgm</code>
<code>show</code>

![Foto](/imgmysql/image022.png)

## Balancejador / Proxy

Hem d'instal·lar les dependències:

![Foto](/capt/1.png)

![Foto](/capt/2.png)

Instal·lem el ProxySQL:

![Foto](/capt/3.png)

![Foto](/capt/4.png)

Iniciem el servei i mostrem el status:

![Foto](/capt/5.png)

Hem d'entrar interfície d'administració i canviar la password del administrador.

![Foto](/capt/6.png)

![Foto](/capt/7.png)

![Foto](/capt/8.png)

![Foto](/capt/9.png)

Entren al mysql creen un usuari donen privilegis i fen un exit.

![Foto](/capt/10.png)